package com.example.projet_prog_mobsdk;

import androidx.appcompat.app.AppCompatActivity;

import android.content.pm.ResolveInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Scanner;
import java.util.HashMap;

import android.content.Intent;

public class AffichageProcess extends AppCompatActivity {

    //map de tous les threads en cours et des runnables associés
    static HashMap<Integer, Pair<Thread, Runnable>> currentThreadRunnable = new HashMap<Integer, Pair<Thread, Runnable>>();
    //Le linearlayout définissant l'interface qui doit être accessible depuis de multiples endroits dans l'activité
    LinearLayout lay;
    //Lancement du monitoring
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Initialisaiton de l'affichage
        setContentView(R.layout.activity_affichage_process);

        lay = (LinearLayout) findViewById(R.id.lin);

        //Creation de l'intent permettant de récupérer les pkg installés
        final Intent mainIntent = new Intent(Intent.ACTION_MAIN,null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        final List pkgAppsList = getPackageManager()
                .queryIntentActivities(mainIntent, 0);
        //Récupération des informations sur les différents processus
        monitor(pkgAppsList);
    }
    //Comportement à la fermeture
    @Override
    protected void onStop() {
        //Interruption de tous les threads et de tous les messages prévus par les Handlers
        for(Pair<Thread, Runnable> pair : currentThreadRunnable.values()){
            pair.first.interrupt();
            updatingRSSHandler.removeCallbacks(pair.second);
        }
        //Effacement des entrées de la table
        currentThreadRunnable.clear();
        super.onStop();
    }
    //Comportement à la pause
    @Override
    protected void onPause() {
        //Interruption de tous les threads et de tous les messages prévus par les Handlers
        for(Pair<Thread, Runnable> pair : currentThreadRunnable.values()){
            pair.first.interrupt();
            updatingRSSHandler.removeCallbacks(pair.second);
        }
        //Effacement des entrées de la table
        currentThreadRunnable.clear();
        super.onPause();
    }
    //A la révouerture
    @Override
    protected void onResume() {
        //Suppression des processus affichés
        lay.removeAllViews();
        //Récupération/mise à jour de la liste des pkg
        final Intent mainIntent = new Intent(Intent.ACTION_MAIN,null);
        mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        final List pkgAppsList = getPackageManager()
                .queryIntentActivities(mainIntent, 0);
        //Récupération des informations sur les pkgs
        monitor(pkgAppsList);

        super.onResume();
    }

    //Récupération infos système
    void monitor (List pkgAppsList) {
        Process process = null;
        try {
            process = new ProcessBuilder("ps").start();
        } catch (IOException e) {
            return;
        }
        InputStream in = process.getInputStream();
        Scanner scanner = new Scanner(in);
        while(scanner.hasNextLine()){
            String line = scanner.nextLine();

            if (line.startsWith("u0_")){
                String[] temp = line.split(" ");
                String packageName = temp[temp.length - 1];

                int pid = 0;
                for (int i = 1 ; i < temp.length ; i++){
                    if(!temp[i].equals("")){
                        pid = new Integer(temp[i]).intValue();
                        Log.d("PID?", temp[i]);

                        break;
                    }
                }
                //memoire qu'occupe le process
                String RSS = temp[temp.length - 5];
                for(Object object :pkgAppsList) {
                    ResolveInfo info = (ResolveInfo) object;
                    String strPackageName = info.activityInfo.applicationInfo
                            .packageName.toString();
                    int UID = info.activityInfo.applicationInfo.uid;
                    //Correspondance PID/UID si l'UID existe il remplace le PID
                    if (strPackageName.equals(packageName)){
                        pid = UID;
                    }
                }
                //Création de la view pour afficher le processus et ses informations
                lay.addView(createProcessView(pid, packageName, RSS));
            }
        }
    }
    //Retourne le RSS de l'application qui a target pour pid
    String update_rss (List pkgAppsList, int target) {
        //int target : pid ou uid du process dont on veut le rss
        //valeur à retourner
        String retval = "0";
        Process process = null;
        try {
            process = new ProcessBuilder("ps").start();
        } catch (IOException e) {

        }
        InputStream in = process.getInputStream();
        Scanner scanner = new Scanner(in);
        while(scanner.hasNextLine()){
            String line = scanner.nextLine();

            if (line.startsWith("u0_")){
                String[] temp = line.split(" ");
                String packageName = temp[temp.length - 1];

                int pid = 0;
                for (int i = 1 ; i < temp.length ; i++){
                    if(!temp[i].equals("")){
                        pid = new Integer(temp[i]).intValue();


                        break;
                    }
                }
                //memoire qu'occupe le process
                String RSS = temp[temp.length - 5];

                for(Object object :pkgAppsList) {
                    ResolveInfo info = (ResolveInfo) object;
                    String strPackageName = info.activityInfo.applicationInfo
                            .packageName.toString();
                    int UID = info.activityInfo.applicationInfo.uid;
                    //Correspondance PID/UID pour s'assurer que l'on ne compare pas un UID avec un PID
                    if (strPackageName.equals(packageName)){

                        pid = UID;
                    }
                    //Si le pid courant est celui du processus d'intérêt on arrête les calculs et on sauvegarde le RSS
                    if(target == pid) {
                        retval = RSS;
                        break;
                    }
                }
            }
        }
        return retval;
    }



    //Création de l'affichage du processus de paramètres UID, app, monitoring value
    RelativeLayout createProcessView (int UID, String app, String monitoring_value) {
        //TextView tx = new TextView(this);
        //Le relative layout permet de placer des éléments dans un rectangle et d'envoyer le rectangle remplis dans la layout global ensuite
        RelativeLayout layout = new RelativeLayout(this);
        //Paramètres de placement du texte
        RelativeLayout.LayoutParams paramsTopLeft =
                new RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
        paramsTopLeft.addRule(RelativeLayout.ALIGN_PARENT_LEFT,
                RelativeLayout.TRUE);
        paramsTopLeft.addRule(RelativeLayout.ALIGN_PARENT_TOP,
                RelativeLayout.TRUE);
        paramsTopLeft.setMarginStart(50);

        //Paramètres de placement du boutton
        RelativeLayout.LayoutParams paramsRight =
                new RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.WRAP_CONTENT,
                        RelativeLayout.LayoutParams.WRAP_CONTENT);
        paramsRight.addRule(RelativeLayout.ALIGN_PARENT_RIGHT,
                RelativeLayout.TRUE);
        paramsRight.setMarginEnd(50);
        //Creation de la textview qui portera les informations de monitoring
        TextView tx = new TextView(this);
        //On donne à la textview un id, cet id est le PID/UID de l'application, de cette manière on peut retrouver la textview facilement
        tx.setId(UID);
        //On inscrit le texte de monitoring
        tx.setText("[" + Integer.toString(UID) + "]" + app + "\n" + "RSS: " + monitoring_value);
        //On ajoute la textview au layout
        layout.addView(tx, paramsTopLeft);

        //Creation du bouton de monitoring
        Button BT2 = new Button(this);
        //ajout du texte au bouton
        BT2.setText("MONITOR");
        //On donne un tag au bouton avec l'UID/PID, de cette manière, quand on clique sur le bouton, on peut reconnaître le processus à monitorer, sinon tous les boutons seraient identiques
        BT2.setTag(0x80000000, UID);

        //Le fond du bouton est gris
        BT2.setBackgroundColor(0xff808080);
        //Listener pour réagir à un clique sur le bouton
        BT2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    //On récupère le PID/UID dans le tag du bouton
                    int UID =(int) v.getTag(0x80000000);
                    //On vérifie dans la map si le processus n'est pas déjà monitoré
                    if(!currentThreadRunnable.containsKey(UID)) {
                        //On lance le monitoring
                        //On crée le runnable avec le PID/UID en paramètre
                        Runnable up = new MyRunnable(UID);
                        //On ajoute le thread et le runnable dans la map pour pouvoir les arrêter plus tard
                        currentThreadRunnable.put(UID, new Pair<Thread, Runnable>(new Thread(up), up));
                        //On démarre le thread
                        currentThreadRunnable.get(UID).first.start();
                        //On crée un message pour le handler lui indiquant de modifier le bouton pour signifier à l'utilisateur qu'on est entrain de monitorer le processus
                        Message msg = updatingColorHandler.obtainMessage(UID, new Pair<View, Integer>(v,0xff00ff00));
                        updatingColorHandler.sendMessage(msg);
                    }
                    else{
                        //Le monitoring est déjà lancé, on l'arrête
                        //Récupération et intérruption du thread
                        currentThreadRunnable.get(UID).first.interrupt();
                        //Suppression des callbacks du runnable par le handler
                        updatingRSSHandler.removeCallbacks(currentThreadRunnable.get(UID).second);
                        //Changement de la couleur du bouton pour signifier qu'on a arrêté le monitoring, ceci à l'aide d'un handler
                        Message msg = updatingColorHandler.obtainMessage(UID, new Pair<View, Integer>(v,0xff808080));
                        updatingColorHandler.sendMessage(msg);
                        //on efface le runnable et le thread de la map
                        currentThreadRunnable.remove(UID);
                    }
                }
                catch(Exception e) {
                }

            }
        });
        //On ajoute le bouton crée au layout
        layout.addView(BT2, paramsRight);
        return layout;
    }
    //Création d'une nouvelle classe implémentant Runnable mais qui prend un paramètre (le PID)
    public class MyRunnable implements Runnable {
        //PID de l'application visée
        int PID;
        public MyRunnable(int pid) {
            PID = pid;

        }
        public void run() {
            //Récupération des infos sur les packages
            final Intent mainIntent = new Intent(Intent.ACTION_MAIN,null);
            mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
            final List pkgAppsList = getPackageManager()
                    .queryIntentActivities(mainIntent, 0);
            //Récupération du RSS mis à jour
            String RSS = update_rss(pkgAppsList, PID);
            //Création du message pour handler, ce dernier va mettre à jour le RSS affiché pour ce processus
            Message msg = updatingRSSHandler.obtainMessage(PID, RSS);
            updatingRSSHandler.sendMessage(msg);
            //Callback après 5s du Runnable, attention, il tourne indéfiniement si on ne fait rien
            updatingRSSHandler.postDelayed(this, 5000);

        }
    }
    //Handler de mise à jour du RSS
    final Handler updatingRSSHandler = new Handler() {
        public void handleMessage(Message msg){
            //Récupération de la textvie grâce à l'id qu'on lui a donné (le pid)
            View tx = findViewById(msg.what);
            //Récupération du texte déjà affiché
            String text = (String)((TextView) tx).getText();
            //On va découper le texte déjà affiché pour uniquement remaplcer la valeur
            int place = text.indexOf("RSS");
            //Récupération du nouveau RSS
            String val = (String)msg.obj;
            if(val.length()>0) {
                //Si le RSS a bien été trouvé
                text = text.substring(0, place) + "RSS: " + msg.obj;
            }
            else{
                //Si le RSS est vide, l'application n'utilise plus de mémoire, on affiche 0
                text = text.substring(0, place) + "RSS: " + "0";
            }
            //On modifie le texte
            ((TextView) tx).setText(text);

        }
    };
    //Changement de la couleur du bouton
    final Handler updatingColorHandler = new Handler() {
        public void handleMessage(Message msg){
            //Récupération du bouton concerné et du code de la couleur
            Pair<View, Integer> pair = (Pair<View, Integer>)msg.obj;
            View button = pair.first;
            //Changement de couleur
            ((Button) button).setBackgroundColor((Integer) pair.second);

        }
    };
}
