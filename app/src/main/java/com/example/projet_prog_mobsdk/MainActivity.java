package com.example.projet_prog_mobsdk;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.content.Intent;
import android.view.View;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn = (Button) findViewById(R.id.BT1);
        btn.setOnClickListener(this);

    }
    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.BT1:
                Button btn = (Button) findViewById(R.id.BT1);
                btn.setText("Ready !");

                Intent playIntent = new Intent(this, AffichageProcess.class);
                //playIntent.putExtra("name", "mon NOM");
                startActivity(playIntent);

                break;
        }
    }

}
